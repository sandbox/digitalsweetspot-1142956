Module: Jump Links
Author: Jon Hopewell (digitalsweetspot)

About Jump Links
=====================================================================
The Jump Links module provides a way to add random, miscellaneous 
links on a per-page basis.  It generates a single block of these 
links which can be placed in any region on your site.
When adding or editing a Jump Link, the following form fields are 
available:

+ Display name: the name of the link as it is displayed to the user

+ Destination URL: the url which the user will be taken to upon 
clicking the link

+ Page(s) displayed on: a listing of pages which will be used to 
determine whether or not a Jump Link will be shown in the block

It is important to note that you may have many Jump Links per page, but 
no two Jump Links can have the same "Display name" and be listed on the 
same page.  However, you can have multiple Jump Links with the same 
"Destination URL" listed on the same page if you wanted to.

For example, on page url "press-release/latest" you can have only 
one link titled "Upcoming Events" which links to 
"events/upcoming", however you could have another link titled 
"Events This Week" which also links to "events/upcoming".

Important note on configuring the Jump Links block "visibility" 
settings:

This block is disabled by default, as we don't assume to know where 
you want to place it.  Be sure to visit the admin/build/block page 
and enable the Jump Links block.

This block is intended to "Show on all pages except listed 
pages", and there typically will be no listed pages.  The nature of 
this block is to be available at all times on every page, and let 
the list of Jump Links determine which pages this block will appear 
on.


Installing Jump Links Module
=====================================================================
+ Copy the 'jump_links' module directory in to your Drupal
sites/all/modules directory as usual.

+ Enable the module at admin/build/modules

+ Set the Permissions to "administer jump links"

+ Visit admin/content/jump_links to administer


Help With Jump Links
=====================================================================
Find help with the Jump Links module at admin/help/jump_links.


Acknowledgements
=====================================================================
Thanks to GLAD WORKS and Paperworks for sponsoring this module.